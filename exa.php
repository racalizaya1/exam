<?php
//herencia
class producto{
    protected $id;
    private $titulo;
    private $precio;
    private $posicion;
    private $estado;

    function __construct($id,$titulo,$precio,$posicion,$estado){
        $this-> id=$id;
        $this-> titulo=$titulo;
        $this-> precio=$precio;
        $this-> posicion=$posicion;
        $this-> estado=$estado;
    }
    public function getautor(){
        return $this->titulo." ".$this->precio." ".$this->posicion." ".$this->estado;
    }
}
class video extends producto{
    private $tiempo;
    function __construct($id,$titulo,$precio,$posicion,$estado,$tiempo){
        parent ::__construct($id,$titulo,$precio,$posicion,$estado);
        $this->tiempo = $tiempo;
    }
    public function gettiempo(){
        return $this->tiempo;
    }
}
$videoa=new video(1,"ReyLeon","precio:", 50," "," ",24);
echo $videoa ->getautor();
echo $videoa ->gettiempo();
//polimorfismo
class devolucion {
    protected $var_protected_sueldo;
    protected $var_protected_impuesto;
    public function setPago($dinero)
    {
        $this->var_protected_dinero=$dinero;
    }
    public function setImpuesto($impuesto)
    {
        $this->var_protected_impuesto=$impuesto;
    }

    public function getdineroImpuesto()
    {
        return $this->var_protected_dinero* $this->var_protected_impuesto;
    }
  }
    class docente extends devolucion{
        public function getdineroImpuesto()
        {
            return $this->var_protected_dinero*($this->var_protected_impuesto + .05);
        }
    }
    $odevolucion= new devolucion();
    $oPago= new docente();

    $odevolucion->setPago(50);
    $odevolucion->setImpuesto(.12);

    $oPago->setPago(50);
    $oPago->setImpuesto(.12);

    echo"el primer video sera devuelto hasta las:: <br>";
    echo $odevolucion-> getdineroImpuesto()."<br>";
    echo "<br>";
    echo"el segundo video sera devuelto hasta las: <br>";
    echo $oPago->getdineroImpuesto()."<br>";

  //interfaces 

     interface tienda{
        function prestar();
    }
    class classprestamoUno implements tienda{
        function prestar(){
            echo "prestamo 1: Rey Leon";
        }
    }
    class classprestamoDos implements tienda{
        function prestar(){
            echo "prestamos 2: Madagascar";
        }
    }
    class classprestamosTres implements tienda{
        function prestar(){
            echo "prestamo 3: jumanji";
        }
    }
    function area(tienda $obj){
    }
    $uno = new classprestamoUno;
    $dos = new classprestamoDos;
    $tres = new classprestamosTres;
    area($uno);
    area($dos);
    area($tres);
?>