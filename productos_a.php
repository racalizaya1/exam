<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Tienda de fajas</title>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="styleusuario.css">
    </head>
    <body>
        <header>
                <div class="divicion">
                    <center><h1>Tienda de fajas online</h1></center>
                     <nav>
                         <ul>
                             <li><a href="tienda.php">gentionar productos</a></li>
                             <li><a href="administrador_usuarios.php">gestionar clientes</a></li>
                             <li><a href="administrador.php">cerrar sesion</a></li>
                         </ul>
                     </nav>
                </div>
        </header>
        <section class="main">
            <form>
                <center>
                    <table bgcolor="crimson" border="3">
                       <thead>
                            <tr>
                                <th colspan="1"><a href="administrador_pagina.php">Nuevo</a></th>
                                <th colspan="8">Lista de Productos</th>
                            </tr>
                       </thead>
                       <tbody>
                           <tr>
                               <td>Id</td>
                               <td>Nombre</td>
                               <td>Categoria</td>
                               <td>Marca</td>
                               <td>Talla</td>
                               <td>Precio</td>
                               <td>Credito</td>
                               <td colspan="2">Operaciones</td>
                           </tr>
                           <?php
                                include("conexion.php");

                                $query="SELECT * FROM productos";
                                $resultado=$conexion->query($query);
                                while($row=$resultado->fetch_assoc()){
                            ?>
                                <tr>
                                     <td><?php echo $row['idarticulo'];?></td>
                                     <td><?php echo $row['nombre'];?></td>
                                     <td><?php echo $row['categoria'];?></td>
                                     <td><?php echo $row['marca'];?></td>
                                     <td><?php echo $row['talla'];?></td>
                                     <td><?php echo $row['precio'];?></td>
                                     <td><?php echo $row['credito'];?></td>
                                     <td><a href="modificar_articulo.php?idarticulo=<?php echo $row['idarticulo']; ?> ">Modificar</a></td>
                                     <td><a href="eliminar_articulo.php?idarticulo=<?php echo $row['idarticulo']; ?> ">Eliminar</a></td>
                                </tr>
                           <?php
                                }
                           ?>
                       </tbody>
                    </table>
                    </center>
            </form>
            <div class="productos">
                       
            </div>
        </section>

        <footer>
            <div>
                <section>
                    
                </section>
            </div>

        </footer>
    </body>