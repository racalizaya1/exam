<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Tienda de fajas</title>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="styleusuario.css">
        <script>
         function solonumeros(e) {
             key=e.keyCode || e.which;
             teclado=String.fromCharCode(key);
             numeros="0123456789";
             especiales="8-37-38-46";
             teclado_especial=false;
             for(var i in especiales){
                 if(key==especiales[i]){
                teclado_especial=true;
                 }
             }
             if(numeros.indexOf(teclado)==-1 && !teclado_especial){
                   return false;
             }
         }
        </script>
         <script>
         function sololetras(e) {
             key=e.keyCode || e.which;
             teclado=String.fromCharCode(key).toLowerCase();
             letras="abcdefghijklmnñopqrstuvwxyz";
             especiales="8-37-38-46-164";
             teclado_especial=false;
             for(var i in especiales){
                 if(key==especiales[i]){
                    teclado_especial=true; break;
                 }
             }
             if(letras.indexOf(teclado)==-1 && !teclado_especial){
                   return false;
             }
         }
        </script>
    </head>
    <body>
        <header>
                <div class="divicion">
                    <center><h1>Tienda de fajas online</h1></center>
                     <nav>
                         <ul>
                             <li><a href="administrador_productos.php">gentionar productos</a></li>
                             <li><a href="administrador_usuarios.php">gestionar clientes</a></li>
                             <li><a href="administrador.php">cerrar sesion</a></li>
                         </ul>
                     </nav>
                </div>
        </header>
        <section class="main"><center>
            <form action="operacion_guardar_producto.php" method="POST" enctype="multipart/form-data">
                <label for="producto">Nombre del producto:</label>
                <input type="text" name="nombre" id="nombre" placeholder="nombre del producto"onkeypress="return sololetras(event)">

                <label for="categoria">Categoria del producto:</label>
                <input type="text" name="categoria" id="categoria" placeholder="Escriba la categoria del prodcuto"onkeypress="return sololetras(event)">

        
                <label for="marca">Marca:</label>
                <input type="text" name="marca" id="marca" placeholder="Marca del producto"onkeypress="return sololetras(event)">

                <label for="talla">Tallas:</label>
                <input type="text" name="talla" id="talla" placeholder="toda las tallas con separacion de 'comas- ,' ">

                
                <label for="precio">Precio:</label>
                <input type="double" name="precio" id="precio" placeholder="Poner precio en bolivianos"onkeypress="return solonumeros(event)">

                
                <label for="credito">Credito:</label>
                <input type="text" name="credito" id="credito" placeholder="cantidad a pagar por mes">

                <input type="file" name="imagen"/>

                <input type="submit" value="Registrar producto">
                <a href="administrador_productos.php">administrar productos</a>
                
            </form>
            <div class="platos">
                       
            </div>
        </center>
        </section>

        <footer>
            <div>
                <section>
                    
                </section>
            </div>

        </footer>
    </body>
</html>